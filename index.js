const http = require('http');
const ngrok = require('ngrok');
const qs = require('qs');
const hl = require('cli-highlight').highlight;

const PORT = 3000;

const log = function () {
  let data = [];
  for (let arg of arguments) {
    if (!['number', 'string'].includes(typeof arg)) {
      arg = hl(JSON.stringify(arg, null, 4), 'json');

    }
    data.push(arg);
  }
  console.log(data.join(' '));
}

const server = http.createServer((req, res) => {
  let data = [];
  req.on('data', chunk => {
    log('>', chunk);
    data.push(chunk);
  }).on('end', () => {
    data = Buffer.concat(data).toString();
    log('METHOD', req.method.toUpperCase());
    log('URL', req.url);
    log('QUERY', qs.parse(req.url.split('?').pop()));
    log('HEADERS', req.headers);
    log('BODY', data);
    log();
    res.end('hi');
  })
});

server.listen(PORT, async () => {
  log();
  log(`Started on ${PORT}`);
  const url = await ngrok.connect(PORT);+

  log('External url', url, '\n');
});

